# -*- coding: utf-8 -*-

#%% Imports
import pygame
import random
import numpy as np

from deap import base
from deap import creator
from deap import tools

from functools import cmp_to_key


#%% Constants
WIDTH, HEIGHT = 1200, 750

WIN = pygame.display.set_mode((WIDTH, HEIGHT))

pygame.display.set_caption("Lizany Marias")

pygame.font.init()

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

FPS = 10

LEVEL_FONT = pygame.font.SysFont("comicsans", 20)

CER_SEDMA  = pygame.image.load("images/cer_sedma.png")
CER_OSMA  = pygame.image.load("images/cer_osma.png")
CER_DEVITKA  = pygame.image.load("images/cer_devitka.png")
CER_DESITKA  = pygame.image.load("images/cer_desitka.png")
CER_SPODEK  = pygame.image.load("images/cer_spodek.png")
CER_SVRSEK  = pygame.image.load("images/cer_svrsek.png")
CER_KRAL  = pygame.image.load("images/cer_kral.png")
CER_ESO  = pygame.image.load("images/cer_eso.png")

ZAL_SEDMA  = pygame.image.load("images/zal_sedma.png")
ZAL_OSMA  = pygame.image.load("images/zal_osma.png")
ZAL_DEVITKA  = pygame.image.load("images/zal_devitka.png")
ZAL_DESITKA  = pygame.image.load("images/zal_desitka.png")
ZAL_SPODEK  = pygame.image.load("images/zal_spodek.png")
ZAL_SVRSEK  = pygame.image.load("images/zal_svrsek.png")
ZAL_KRAL  = pygame.image.load("images/zal_kral.png")
ZAL_ESO  = pygame.image.load("images/zal_eso.png")

LIS_SEDMA  = pygame.image.load("images/lis_sedma.png")
LIS_OSMA  = pygame.image.load("images/lis_osma.png")
LIS_DEVITKA  = pygame.image.load("images/lis_devitka.png")
LIS_DESITKA  = pygame.image.load("images/lis_desitka.png")
LIS_SPODEK  = pygame.image.load("images/lis_spodek.png")
LIS_SVRSEK  = pygame.image.load("images/lis_svrsek.png")
LIS_KRAL  = pygame.image.load("images/lis_kral.png")
LIS_ESO  = pygame.image.load("images/lis_eso.png")

KUL_SEDMA  = pygame.image.load("images/kul_sedma.png")
KUL_OSMA  = pygame.image.load("images/kul_osma.png")
KUL_DEVITKA  = pygame.image.load("images/kul_devitka.png")
KUL_DESITKA  = pygame.image.load("images/kul_desitka.png")
KUL_SPODEK  = pygame.image.load("images/kul_spodek.png")
KUL_SVRSEK  = pygame.image.load("images/kul_svrsek.png")
KUL_KRAL  = pygame.image.load("images/kul_kral.png")
KUL_ESO  = pygame.image.load("images/kul_eso.png")

TURN  = pygame.image.load("images/turn.png")

CARD_WIDTH = 60
CARD_HEIGHT = 80

COLORS = [
    'cer',
    'lis',
    'zal',
    'kul'
]
VALUES = [
    'sedma',
    'osma',
    'devitka',
    'desitka',
    'spodek',
    'svrsek',
    'kral',
    'eso'
]
COLOR_PREF = {
    'kul': 1,
    'zal': 2,
    'lis': 3,
    'cer': 4
}
VALUE_PREF = {
    'sedma': 7,
    'osma': 8,
    'devitka': 9,
    'spodek': 10,
    'svrsek': 11,
    'kral': 12,
    'desitka': 13,
    'eso': 14
}


#%% Card
class Card:
    def __init__(self, color, value):
        self.color = color
        self.value = value
        self.picture = None
        self.points = 0

        if value == 'desitka' or value == 'eso':
            self.points = 10

        if color == 'cer' and value == 'sedma':
            self.picture = CER_SEDMA
        if color == 'cer' and value == 'osma':
            self.picture = CER_OSMA
        if color == 'cer' and value == 'devitka':
            self.picture = CER_DEVITKA
        if color == 'cer' and value == 'desitka':
            self.picture =  CER_DESITKA
        if color == 'cer' and value == 'spodek':
            self.picture = CER_SPODEK
        if color == 'cer' and value == 'svrsek':
            self.picture = CER_SVRSEK
        if color == 'cer' and value == 'kral':
            self.picture = CER_KRAL
        if color == 'cer' and value == 'eso':
            self.picture = CER_ESO

        if color == 'zal' and value == 'sedma':
            self.picture = ZAL_SEDMA
        if color == 'zal' and value == 'osma':                                   
            self.picture = ZAL_OSMA
        if color == 'zal' and value == 'devitka':
            self.picture = ZAL_DEVITKA
        if color == 'zal' and value == 'desitka':
            self.picture = ZAL_DESITKA
        if color == 'zal' and value == 'spodek':
            self.picture = ZAL_SPODEK
        if color == 'zal' and value == 'svrsek':
            self.picture = ZAL_SVRSEK
        if color == 'zal' and value == 'kral':
            self.picture = ZAL_KRAL
        if color == 'zal' and value == 'eso':
            self.picture = ZAL_ESO  

        if color == 'lis' and value == 'sedma':
            self.picture = LIS_SEDMA
        if color == 'lis' and value == 'osma':
            self.picture = LIS_OSMA
        if color == 'lis' and value == 'devitka':
            self.picture = LIS_DEVITKA
        if color == 'lis' and value == 'desitka':
            self.picture = LIS_DESITKA
        if color == 'lis' and value == 'spodek':
            self.picture = LIS_SPODEK
        if color == 'lis' and value == 'svrsek':
            self.picture = LIS_SVRSEK
        if color == 'lis' and value == 'kral':
            self.picture = LIS_KRAL
        if color == 'lis' and value == 'eso':
            self.picture = LIS_ESO

        if color == 'kul' and value == 'sedma':
            self.picture = KUL_SEDMA
        if color == 'kul' and value == 'osma':
            self.picture = KUL_OSMA
        if color == 'kul' and value == 'devitka':
            self.picture = KUL_DEVITKA
        if color == 'kul' and value == 'desitka':
            self.picture = KUL_DESITKA
        if color == 'kul' and value == 'spodek':
            self.picture = KUL_SPODEK
        if color == 'kul' and value == 'svrsek':
            self.picture = KUL_SVRSEK
        if color == 'kul' and value == 'kral':
            self.picture = KUL_KRAL
        if color == 'kul' and value == 'eso':
            self.picture = KUL_ESO


    def __repr__(self):
        return "Card:" + self.color + ' ' + self.value

    def __str__(self):
        return self.color + ' ' + self.value


COMPLETE_CARD_LIST = [Card(b, h) for b in COLORS for h in VALUES]


#%% Stych
class Stych:
    def __init__(self, phase, card0, card1, hlaska0, hlaska1, wt):
        self.card0 = card0
        self.card1 = card1
        self.cards = [card0, card1]
        self.hlaska0 = hlaska0 # true/false, pokud karta byla hlaska
        self.hlaska1 = hlaska1
        self.hlasky = [hlaska0, hlaska1] # pro pristupn skrze pole
        self.wt = wt # who took
        self.phase = phase


#%% History
class History:
    def __init__(self):
        self.stychy = []

    def add(self, phase, card0, card1, hlaska0, hlaska1, wt):
        self.stychy.append(Stych(phase, card0, card1, hlaska0, hlaska1, wt))

    def getlast(self):
        return self.stychy[-1] if len(self.stychy) else None


#%% Talon
class Talon:
    def __init__(self, cards):
        self.cards = cards
    
    def shuffle(self):
        random.shuffle(self.cards)
    
    def isempty(self):
        if len(self.cards) == 0:
            return True
        return False
    
    def popcard(self):
        if not self.isempty():
            return self.cards.pop()
        return None


#%% Hand
class Hand:
    def __init__(self):
        self.cards = []

    def __cmpcards__(self, card1, card2):
        c1_color_pref = COLOR_PREF[card1.color]
        c2_color_pref = COLOR_PREF[card2.color]
        c1_value_pref = VALUE_PREF[card1.value]
        c2_value_pref = VALUE_PREF[card2.value]

        if c1_color_pref < c2_color_pref:
            return 1
        elif c1_color_pref == c2_color_pref:
            if c1_value_pref < c2_value_pref:
                return 1
            elif c1_value_pref == c2_value_pref:
                return 0
            else:
                return -1
        else:
            return -1

    def sort(self):
        self.cards = sorted(self.cards, key=cmp_to_key(self.__cmpcards__))
    
    def addcard(self, card):
        self.cards.append(card)
        # neefektivni - tridi se pri kazdem liznuti karty
        self.sort()

    def isempty(self):
        if len(self.cards) == 0:
            return True
        return False 

    def iscardpresent(self, col, val):
        for c in self.cards:
            if c.color == col and c.value == val:
                return True
        return False

    # vraci vsechny karty dane barvy
    def getcardscol(self, col):
        l = []
        for c in self.cards:
            if c.color == col:
                l.append(c)
        return l

    # vraci vsechny karty dane barvy vyssi nez value
    def getcardshigher(self, col, val):
        l = []
        for c in self.cards:
            if c.color == col and VALUE_PREF[c.value] > VALUE_PREF[val]:
                l.append(c)
        return l

    # vraci vsechny karty dane hodnoty
    def getcardsval(self, val):
        l = []
        for c in self.cards:
            if c.value == val:
                l.append(c)
        return l

    def removecard(self, col, val):
        index = -1
        if self.iscardpresent(col, val):
            for i in range(len(self.cards)):
                if (self.cards[i].color == col) and (self.cards[i].value == val):
                    index = i
                    break
        if index >=0:
            del self.cards[i]

    def disp(self):
        for c in self.cards:
            c.disp()

    # returns valid card moves to opcard
    def validcardmoves(self, opcard, phase, trumfcolor):
        #opcard.disp()
        #print("---")
        #print(phase)
        #print(trumfcolor)
        #print("---")

        if self.isempty():
            return []

        cardmoves = self.cards.copy()

        if opcard != None:
            cardmoves = self.getcardshigher(opcard.color, opcard.value)

            if len(cardmoves) == 0:
                # hrac nema zadne vyssi karty dane barvy
                # alespon chceme karty stejne barvy
                cardmoves = self.getcardscol(opcard.color)

                if len(cardmoves) == 0:
                # hrac nema karty v barve protihracovy karty
                    if phase == 1:
                        # v prvni fazi muzeme hrat cokoli, pokud nemame barvu
                        # ve druhe fazi, kdyz nemame, musime trumfovat
                        cardmoves = self.getcardscol(trumfcolor)
                        if len(cardmoves) == 0:
                            cardmoves = self.cards.copy()
                    else:
                         cardmoves = self.cards.copy()

        # vyhozeni kralu z moznych tahu, kdyz mame i svrska
        # musime hrat svrska prvniho
        svrsci = self.getcardsval('svrsek')
        for s in svrsci:
            index = -1
            for i in range(len(cardmoves)):
                if cardmoves[i].color == s.color and cardmoves[i].value == 'kral':
                    index = i
                    break

            if index >= 0:
                del cardmoves[i]

        return cardmoves 

    # vybere nahodnou kartu
    def chooserandom(self, cardlist):
        return random.choice(cardlist)


#%% Player
class Player:
    def __init__(self, num, name):
        self.num = num
        self.name = name
        self.hand = Hand()
        self.scoreDifference = 0

    def isdone(self):
        if self.hand.isempty():
            return True
        return False

    def addcard(self, card):
        self.hand.addcard(card)

    def play(self, history, phase, trumfcolor, first = True, opcard = None):
        if not self.isdone():
            hlaska = False

            if first:
                cm = self.hand.validcardmoves(None, phase, trumfcolor)
                #print(cm)
                # tisk validnich tahu
                #print("====================================")
                #print("Hrac:", self.name, "===============")
                #print("------ Ma v ruce:")
                #for c in self.hand.cards:
                #    print(c)
                #print("------ Muze hrat")
                #for c in cm:
                #    print(c)

                c = self.hand.chooserandom(self.hand.cards)

                if c.value == 'svrsek':
                    if self.hand.iscardpresent(c.color, 'kral'):
                        hlaska = True

                self.hand.removecard(c.color, c.value)

                return c, hlaska
            else:
                # hraje jako druhy
                cm = self.hand.validcardmoves(opcard, phase, trumfcolor)

                # tisk validnich tahu
                #print("====================================")
                #print("Hrac:", self.name, "===============")
                #print("------ Ma v ruce:")
                #for c in self.hand.cards:
                #    print(c)
                #print("------ Muze hrat")
                #for c in cm:
                #    print(c)

                c = self.hand.chooserandom(cm)

                if c.value == 'svrsek':
                    if self.hand.iscardpresent(c.color, 'kral'):
                        hlaska = True

                self.hand.removecard(c.color, c.value)

                return c, hlaska

        return None, False

#TODO Fáze 1
N=50  #počet neuronů (50)
O=5 #z 5 karet co mas v ruce hrajes tu nejvice ohodnocenou
I=1+(1+5+28)*2 # *2(barvy a hodnoty), 1+ = barva trumfu, (1+ vylozena souperova karta, 5 karty v nasi ruce, 28 pocet moznych souperovych tahu
DELKA_JEDINCE = I * N + N * O + N  # záleží na počtu vah a prahů u neuronů

def nn_function(inp, wei):
    gap=[]
    weight=0
    for neurons in range(0,N):#pro kazdy neuron
        weightSum=0
        for vstupi in range(0,len(inp)):#secte vstupy vynasobene vahami
            weightSum+=inp[vstupi]*wei[weight]
            weight+=1
        if(weightSum>wei[len(wei)-N+neurons]):
            gap.append(weightSum)
        else:
            gap.append(0)
    out=[]
    for outputs in range(0,O):#pro kazdy vystupni neuron
        suma=0
        for i in range(0,N):#projde pro kazdy vystupneuronu
            suma+=wei[weight]*gap[i]
            weight+=1
        out.append(suma)
        
#řadí od nejmenší hodnoty indexy do pole indexu napr. [4,5,1,2,3,0] Nejlepsi index je tedy value na indexu 5
    outIndex = [0]    
    inserted = False
    for i in range(1,len(out)):
        inserted = False
        for j in range(0,(len(outIndex))):
            if(out[i]>out[outIndex[j]]):
                outIndex.insert(j, i)
                inserted=True
                break
        if not inserted:
            outIndex.append(i)
            
    print("outIndex indexes: ", outIndex)   
    print("Values of out: ",out)
    print("The biggest index value:", out.index(max(out)))
    
    return outIndex

# zresetuje vsechny mes zpatky na start
def reset_mes(mes, pop):
    for i in range(len(pop)):
        me = mes[i]
        me.sequence = pop[i]
        me.fitness = 0

# inicializuje me v poctu num na start 
def set_mes(num,pop):
    l = []
    for i in range(num):
        m = MyPlayer(i%2,'nn_player'+str(i),pop[i])
        l.append(m)
        
    return l
#%% AI player with phases (nn + minimax)
#TODO Fáze 2
class MyPlayer(Player):
    def __init__(self, num, name, sequence):
        super().__init__(num, name)

        #self.this(num,name,np.zeros(DELKA_JEDINCE))
        self.oponentspossiblecards = COMPLETE_CARD_LIST.copy()
        self.sequence=sequence#np.zeros(DELKA_JEDINCE)
        self.scoreDifference = 0
        self.minimaxCounter=0
        self.minimaxLimit = 300
    def oponentsnum(self):
        return (self.num + 1) % 2
    
    #Vybiram mozne karty ze souperovo ruky
    def updateoponentspossiblecards(self,history,handCards):
    #old    self.oponentspossiblecards = [card for card in self.oponentspossiblecards if all((card.value, card.color) != (c.value, c.color) for c in cardstoremove)]
        self.oponentspossiblecards=COMPLETE_CARD_LIST.copy()
        for s in history.stychy:
            for j in self.oponentspossiblecards:
                if(s.card0.color==j.color and s.card0.value==j.value):
                    self.oponentspossiblecards.remove(j)
                if(s.card1.color==j.color and s.card1.value==j.value):
                    self.oponentspossiblecards.remove(j)
        for i in handCards:
            for j in self.oponentspossiblecards:
                if(i.color==j.color and i.value==j.value):
                    self.oponentspossiblecards.remove(j)
        
        cards = self.oponentspossiblecards
        oponentsCard = [str(card).replace("Card:", "") for card in cards]
        print("Oponentovo mozne karty: ",oponentsCard)
        #TODO Fáze 3
        #  Neuron Network AI
    def bestfirstphasemove(self, cm, phase, trumfcolor, first, opcard):     
        #I=1+(1+5+28)*2 # *2(barvy a hodnoty), 1+ = barva trumfu, (1+ vylozena souperova karta, 5 karty v nasi ruce, 28 pocet moznych souperovych tahu
        inputs = np.zeros(I)
        inputs[0] = COLOR_PREF[trumfcolor]
        if not first:    
            inputs[1] = COLOR_PREF[opcard.color]
            inputs[2] = VALUE_PREF[opcard.value]
        index = 3
        for card in self.hand.cards:
            inputs[index] = COLOR_PREF[card.color]
            index+=1
            inputs[index] = VALUE_PREF[card.value]
            index+=1
        index = 13
        for card in self.oponentspossiblecards:
            inputs[index] = COLOR_PREF[card.color]
            index+=1
            inputs[index] = VALUE_PREF[card.value]
            index+=1
        print("inputs",inputs)
        for i in nn_function(inputs,self.sequence):#vstup + vahy
            if self.hand.cards[i] in cm:
                return self.hand.cards[i]
            
        return self.hand.chooserandom(cm)
    #TODO Fáze 4
    #  MiniMax AI
    def bestsecondphasemove(self, cm, phase, trumfcolor, first, opcard):
        self.minimaxCounter = 0
        tah = self.maxf(cm, self.oponentspossiblecards, phase, trumfcolor,first, opcard, 0, 1)[1]
        if tah == None:
            return self.hand.chooserandom(cm)
        else:
            return tah
        
    def play(self, history, phase, trumfcolor, first=True, opcard=None):
        # lastmove = history.getlast()
        # lastcard = None if (lastmove is None) else lastmove.cards[self.oponentsnum()]
    #old    self.updateoponentspossiblecards(self.hand.cards + [lastcard] if lastcard is not None else self.hand.cards[:])
        self.updateoponentspossiblecards(history,self.hand.cards)
        if not self.isdone():
            hlaska = False
            cm = []

            if first:
                cm = self.hand.validcardmoves(None, phase, trumfcolor)
            else:
                cm = self.hand.validcardmoves(opcard, phase, trumfcolor)
                
            c = None
            if phase == 0:
                c = self.bestfirstphasemove(cm,phase,trumfcolor,first,opcard)
            else:
                c = self.bestsecondphasemove(cm, phase, trumfcolor, first, opcard)

            if c.value == 'svrsek':
                if self.hand.iscardpresent(c.color, 'kral'):
                    hlaska = True

            self.hand.removecard(c.color, c.value)

            return c, hlaska
        

        return None, False
        
    def firstwon(self, card0,card1,phase,trumfcolor):
        if phase == 0:
            if (card1.color == card0.color) and (VALUE_PREF[card1.value] > VALUE_PREF[card0.value]):
                return False
            
            else:
                return True
            
        if phase == 1:
            if (card1.color == card0.color) and (VALUE_PREF[card1.value] > VALUE_PREF[card0.value]):
                return False
            elif (card1.color != card0.color) and (card1.color == trumfcolor):
                return False
            else:
                return True
#TODO Fáze 5
    def getscore(self,card0,card1,phase,trumfcolor,player):#Ohodnoceni minimaxu
        score=0
        if(card0.value==VALUES[3] or card0.value==VALUES[7]):
            score+=10
        if(card1.value==VALUES[3] or card1.value==VALUES[7]):
            score+=10
        if(self.firstwon(card0,card1,phase,trumfcolor) and player==1):#Konec ... vyhral hrac 1
            return score
        elif(self.firstwon(card0,card1,phase,trumfcolor) and player!=1):#Konec ... vyhral hrac 2
            return -score
        elif(player!=1):
            return score
        else:
            return -score
#TODO Fáze 6
    def getoptions(self,player0cards,player1cards,phase,trumfcolor,firstplayed,opcard,player):
        opts=[]
        if(firstplayed):
            possiblecards=player0cards
        else:
            h=Hand()
            h.cards=player0cards
            possiblecards=h.validcardmoves(opcard,phase,trumfcolor)
        
        for i in range(0,len(possiblecards)):
            score=0
            if(not firstplayed):
                score=self.getscore(opcard,possiblecards[i],phase,trumfcolor,player)
            #líznutí
            if(len(player0cards)==5 and len(player1cards)!=5):
                for j in range(0,len(player1cards)):
                    pl0cd=player0cards.copy()
                    pl0cd.remove(possiblecards[i])
                    pl1cd=player1cards.copy()
                    pl0cd.append(player1cards[j])
                    pl1cd.remove(player1cards[j])
                    #player1cards[:j] + player1cards[(j-1):]
                    opts.append((possiblecards[i],pl0cd,pl1cd,not firstplayed,score))
            elif(len(player1cards)==5 and len(player0cards)!=5):
                for j in range(0,len(player0cards)):
                    if(possiblecards[i].color==player0cards[j].color and possiblecards[i].value==player0cards[j].value):
                        1+1
                    else:
                        pl0cd=player0cards.copy()
                        pl0cd.remove(possiblecards[i])
                        pl1cd=player1cards.copy()
                        pl1cd.append(player0cards[j])
                        pl0cd.remove(player0cards[j])
                        
                        opts.append((possiblecards[i],pl0cd,pl1cd,not firstplayed,score))
            else:
                pl0cd=player0cards.copy()
                pl0cd.remove(possiblecards[i])
                pl1cd=player1cards.copy()
                opts.append((possiblecards[i],pl0cd,pl1cd,not firstplayed,score))
            #player0cards.remove(possiblecards[i])
       
        return opts
    
    def minf(self,player0cards,player1cards,phase,trumfcolor,firstplayed,opcard,score,player):
        if self.minimaxCounter > self.minimaxLimit:
            #print("Presahl jsem a minimax byl prerusen")#omezeni kdyz bezi moc dlouho
            return (score,None)#👩‍⚖️🙆‍♂️🙄
        self.minimaxCounter = self.minimaxCounter + 1 
        #print("n",len(player0cards),len(player1cards))
        first=True
        if(len(player0cards)==0 or len(player1cards)==0):
            return (score,None)
        
        opts=self.getoptions(player0cards,player1cards,phase,trumfcolor,firstplayed,opcard,player)#Vsechny mozny tahy
        
        if(len(opts)>0):#Mám možnosti
            for op in opts:
                if((player==1 and op[4]>=0)or(player!=1 and op[4]<0)):#hrál první hráč a vyhrál, nebo hrál druhý a prohrál (zacina hrat prvni hrac)
                    a=self.maxf(op[1],op[2],phase,trumfcolor,op[3],op[0],score+op[4],1)[0]
                else:
                    a=self.minf(op[2],op[1],phase,trumfcolor,op[3],op[0],score+op[4],2)[0]#hraje druhý
                
                if(first):#Neni zadna hodnota, da prvni hodnotu.. (vychozi hodnota)
                    minimum=(a,op[0])
                    first=False
    
                if(a>minimum[0]):
                    minimum=(a,op[0])
           
            return minimum
        return (0,None)
    
    #(self.hand,moznesouperovykarty,phase,trumfcolor,first,opcard,0), hráč 1 - max
    def maxf(self,player0cards,player1cards,phase,trumfcolor,firstplayed,opcard,score,player):
        if self.minimaxCounter > self.minimaxLimit:
            #print("Presahl jsem a minimax byl prerusen")
            return (score,None)#👩‍⚖️🙆‍♂️🙄
        self.minimaxCounter += 1
        first=True
        if(len(player0cards)==0 or len(player1cards)==0):#  Konec minimaxu kdyz nemaji karty hraji s 5 vs 5 karty
            return (score,None)
        
        opts=self.getoptions(player0cards,player1cards,phase,trumfcolor,firstplayed,opcard,player) # vsechny moznosti
        
        if(len(opts)>0):
            for op in opts:
                if((player==1 and op[4]>=0)or(player!=1 and op[4]<0)):#hrál první hráč a vyhrál, nebo hrál druhý a prohrál
                    a=self.maxf(op[1],op[2],phase,trumfcolor,op[3],op[0],score+op[4],1)[0]#hraje prvni - 1
                else:
                    a=self.minf(op[2],op[1],phase,trumfcolor,op[3],op[0],score+op[4],2)[0]#hraje druhý - 2
                
                if(first):
                    maximum=(a,op[0])
                    first=False
                
                if(a<maximum[0]):
                    maximum=(a,op[0])
            
            return maximum
        return (0,None)



#%% Marias
class Marias:
    def __init__(self,player1,player0,zeroweights):
        # init karet
        cards = [Card(b, h) for b in COLORS for h in VALUES]
        self.talon = Talon(cards)
        self.talon.shuffle()
        #TODO: Mozno obnovit self.player0 = Player(0, 'Tunta') 
        
        if(zeroweights): 
            self.player0 = MyPlayer(0, 'AI - Oponent',np.zeros(DELKA_JEDINCE))
            self.player1 = MyPlayer(1, 'AI - MyPlayer',np.zeros(DELKA_JEDINCE))
        else:
            self.player0 = player0
            self.player1 = player1
        
        # rozdej
        for i in range(5): 
            self.player0.addcard(self.talon.popcard())
        
        for i in range(5):
            self.player1.addcard(self.talon.popcard())
            
        self.trumfcolor = self.talon.cards[0].color    
        self.lastcard = self.talon.cards[0]

        # posledni tah
        self.lastcard0 = None
        self.lastcard1 = None
        self.lasthlaska0 = False
        self.lasthlaska1 = False

        self.phase = 0 # 0... pripravna faze, 1...dolizany balik 
        self.playerturn = 0
        
        self.player0points = 0 # body hracu
        self.player1points = 0
            
        self.round = 0
        self.history = History()   
    
        
    def isdone(self):
        if self.player0.isdone() and self.player1.isdone():
            return True
        return False
    
        
    def firsttakes(self, card0, card1):
        
        if (card0 == None) or (card1 == None):
            return None
        
        if self.phase == 0:
            if (card1.color == card0.color) and (VALUE_PREF[card1.value] > VALUE_PREF[card0.value]):
                return False
            else:
                return True
            
        if self.phase == 1:
            if (card1.color == card0.color) and (VALUE_PREF[card1.value] > VALUE_PREF[card0.value]):
                return False
            elif (card1.color != card0.color) and (card1.color == self.trumfcolor):
                return False
            else:
                return True
    
    def play(self):
        
        if self.talon.isempty():
            self.phase = 1
        
        # tahy hracu
        if self.playerturn == 0:                
            self.lastcard0, self.lasthlaska0 = self.player0.play(self.history, self.phase, self.trumfcolor, True, None)
            self.lastcard1, self.lasthlaska1 = self.player1.play(self.history, self.phase, self.trumfcolor, False, self.lastcard0)
            
        else:
            self.lastcard1, self.lasthlaska1 = self.player1.play(self.history, self.phase, self.trumfcolor, True, None)
            self.lastcard0, self.lasthlaska0 = self.player0.play(self.history, self.phase, self.trumfcolor, False, self.lastcard1)
            
            
        self.round += 1
        
        c0 = self.lastcard0
        c1 = self.lastcard1     
        
        # kdo bere stych
        wt = None
        if self.playerturn == 0:
            ft = self.firsttakes(c0, c1)
            if ft:
                wt = 0
            else: 
                wt = 1
        else:
            ft = self.firsttakes(c1, c0)
            if ft:
                wt = 1
            else: 
                wt = 0
        
        self.history.add(self.phase, self.lastcard0, self.lastcard1, self.lasthlaska0, self.lasthlaska1, wt)

        #print("********************")
        #print(c0, " vs. ", c1, "wins: ", wt)
        #print("********************")

        if wt == 0:
            self.player0points += c0.points + c1.points
            self.playerturn = 0
            
            if not self.talon.isempty():
                # prvni dolizava hrac, ktery sebral stych
                self.player0.addcard(self.talon.popcard())
                self.player1.addcard(self.talon.popcard())    
            
            # posledni stych
            if self.isdone():
                self.player0points += 10
             
        else:
            self.player1points += c0.points + c1.points
            self.playerturn = 1
            
            if not self.talon.isempty():
                self.player1.addcard(self.talon.popcard())
                self.player0.addcard(self.talon.popcard())
            
            # posledni stych
            if self.isdone():
                self.player1points += 10
            
                
        if self.lasthlaska0:
            if self.lastcard0.color == self.trumfcolor:
                self.player0points += 40
            else:
                self.player0points += 20
        
        if self.lasthlaska1:
            if self.lastcard1.color == self.trumfcolor:
                self.player1points += 40
            else:
                self.player1points += 20
        
        self.lastcard0 = None
        self.lastcard1 = None
        self.lasthlaska0 = False
        self.lasthlaska1 = False
        
        

def draw_window(marias):
    #WIN.blit(SEA, (0, 0))   
    WIN.fill(WHITE)

    
    h1 = LEVEL_FONT.render("Hrac 0: " + marias.player0.name + " ::: Skore: " + str(marias.player0points), 1, BLACK)   
    h2 = LEVEL_FONT.render("Hrac 1: " + marias.player1.name + " ::: Skore: " + str(marias.player1points), 1, BLACK)   
    h3 = LEVEL_FONT.render("Trumfy: ", 1, BLACK)   

    
    WIN.blit(h1, (30, 30))
    WIN.blit(h2, (30, 660))
    WIN.blit(h3, (1000, 10))



    #WIN.blit(FLAG, (WIDTH - ME_SIZE, HEIGHT - ME_SIZE - 10))    
         
    #for mine in mines:
    #    WIN.blit(ENEMY, (mine.rect.x, mine.rect.y))
    
    # cards of player 1 (top) 
    x = 50    
    y = 70
    for c in marias.player0.hand.cards:
        WIN.blit(c.picture, (x, y))
        x += 70
        
        
    # cards of player 2 (down)
    x = 50    
    y = 550
    for c in marias.player1.hand.cards:
        WIN.blit(c.picture, (x, y))
        x += 70
               
        
    # draw history 
    x = 50
    y = 250
    for stych in marias.history.stychy:
        c0 = stych.card0
        c1 = stych.card1
        
        if c0 != None:
            d = 0
            if stych.hlaska0:
                d = 20
            WIN.blit(c0.picture, (x, y-d) )
        
        if c1 != None:
            d = 0
            if stych.hlaska1:
                d = 20
            WIN.blit(c1.picture, (x, y+120+d) )
        
    
        wt = stych.wt
        
        if wt == 0:
            WIN.blit(TURN, (x+20, y-50) )
        if wt == 1:
            WIN.blit(TURN, (x+20, y+250) )
        
        x += 70
        
    WIN.blit(marias.lastcard.picture, (1000, 50))
    pygame.draw.line(WIN, BLACK, (814, 200), (814, 520), 3)
        
    pygame.display.update()
    

# třída reprezentující nejlepšího jedince - hall of fame   
class Hof:
    def __init__(self):
        self.sequence = []
#TODO Fáze 6
# ---------------------------------------------------------------------------
# fitness funkce výpočty jednotlivců
#----------------------------------------------------------------------------
def handle_mes_fitnesses(mes):        
    
    
    #vyhodnoceni tahu jedincum prirazujeme fitness body
    for me in mes: 
        me.fitness=me.scoreDifference
        
        

# uloží do hof jedince s nejlepší fitness
def update_hof(hof, mes):
    l = [me.fitness for me in mes]
    ind = np.argmax(l)
    hof.sequence = mes[ind].sequence.copy()
    

def evoluce():
    # =====================================================================
    # <----- Parametry nastavení evoluce !!!!!
     
    VELIKOST_POPULACE = 40 #(40)
    NGEN = 100       # počet generací (100)
    CXPB = 0.6          # pravděpodobnost crossoveru na páru
    MUTPB = 0.2        # pravděpodobnost mutace
    
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    
    creator.create("Individual", list, fitness=creator.FitnessMax)
    
    toolbox = base.Toolbox()

    toolbox.register("attr_rand", random.random)
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_rand, DELKA_JEDINCE)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    # vlastni random mutace
    def mutRandom(individual, indpb):
        for i in range(len(individual)):
            if random.random() < indpb:
                individual[i] = random.random()
        return individual,

    #NN toolboxes
    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", mutRandom, indpb=0.05)
    toolbox.register("select", tools.selRoulette)
    toolbox.register("selectbest", tools.selBest)
    pop = toolbox.population(n=VELIKOST_POPULACE)
    mes = set_mes(VELIKOST_POPULACE,pop)    
    hof = Hof()
    
    herCelkem = 0
    herVyhranych = 0
    for x in range(0,NGEN):
        for d in range(0, len(mes)):
            print("Genereace:", x)
            marias = Marias(mes[d], mes[d],False)
            run = True
            while run:                 
                if not marias.isdone(): 
                    marias.play()             
                if marias.isdone():
                    run = False
                    
                # <---- ZDE druhá část evoluce po simulaci  !!!!!
                # přepočítání fitness funkcí, dle dat uložených v jedinci
                #funguje to pro pripad, kdy hraje neuronka proti randomu ##&&@@
            mes[d].scoreDifference = marias.player1points - marias.player0points
            herCelkem += 1
            if(marias.player1points > marias.player0points):
                herVyhranych+=1
            handle_mes_fitnesses(mes)   # <--------- ZDE funkce výpočtu fitness !!!!
            update_hof(hof, mes)
            
        # přiřazení fitnessů z jedinců do populace
        # každý me si drží svou fitness, a každý me odpovídá jednomu jedinci v populaci
        for i in range(len(pop)):
            ind = pop[i]
            me = mes[i]
            ind.fitness.values = (me.fitness, )
        # selekce a genetické operace
        offspring = toolbox.select(pop, len(pop))
        offspring = list(map(toolbox.clone, offspring))
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < CXPB:
                toolbox.mate(child1, child2)
        
        for mutant in offspring:
            if random.random() < MUTPB:
                toolbox.mutate(mutant)  
            
        pop[:] = offspring
        
    print("Celkem her:", herCelkem, " z toho vyhranych: ", herVyhranych)
    print("Konec evoluce")
    return hof
    
                   
# hlavni smycka hry
#TODO Fáze posledni
def main():
    AIPlayer = evoluce()
    hofHer = 0
    hofVyher = 0
    cycle = 3
    AIcelkem=0
    
    for j in range(0,cycle):    
        for i in range(0,1000):
            if testPlay(AIPlayer, False):
                hofVyher += 1
            hofHer +=1
        if hofHer > (hofHer-hofVyher):
            AIcelkem += 1
    
        #print("AI1: ", AIcelkem, " AI2: ", cycle-AIcelkem)
    print("AI1 won:", ((hofHer-hofVyher)/hofHer)*100, "%  AI2 won: ", (hofVyher/hofHer)*100, "%")
    print("LAST GAME: AI1:", (hofHer-hofVyher), " AI2: ", hofVyher)
    
    
    # for i in range(0,1000):
    #     if testPlay(AIPlayer, False):
    #         hofVyher += 1
    #     hofHer +=1
    
    # print("Player won:", hofHer-hofVyher, " AI won: ", hofVyher)
    for i in range(0,3):
        testPlay(AIPlayer, True)
    pygame.quit()    
    
def testPlay(player, playWithGrafic):
    marias = Marias(MyPlayer(1, 'AI',player.sequence),MyPlayer(0, 'AI - oponent',player.sequence), False)
    clock = pygame.time.Clock()
    run = True    
    while run:  
        if playWithGrafic:
            clock.tick(FPS)
                        
        
        if playWithGrafic:
            keys_pressed = pygame.key.get_pressed()
        
        # herni kolo - stridaji se tahy hracu
        
        if playWithGrafic:
            if keys_pressed[pygame.K_SPACE] and not marias.isdone(): 
                marias.play()
        else:                
            if not marias.isdone(): 
                marias.play()
            
        
        if playWithGrafic:                                  
            draw_window(marias)

        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
    
        if not playWithGrafic:
            if marias.isdone():
                run = False
    return marias.player1points > marias.player0points


if __name__ == "__main__":
    main()