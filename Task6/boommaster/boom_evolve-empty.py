# -*- coding: utf-8 -*-


import pygame
import numpy as np
import random
import math


from deap import base
from deap import creator
from deap import tools


pygame.font.init()


#-----------------------------------------------------------------------------
# Parametry hry 
#-----------------------------------------------------------------------------

WIDTH, HEIGHT = 900, 500

WIN = pygame.display.set_mode((WIDTH, HEIGHT))

WHITE = (255, 255, 255)


TITLE = "Boom Master"
pygame.display.set_caption(TITLE)

FPS = 80
ME_VELOCITY = 10
MAX_MINE_VELOCITY = 3



BOOM_FONT = pygame.font.SysFont("comicsans", 100)   
LEVEL_FONT = pygame.font.SysFont("comicsans", 20)   



ENEMY_IMAGE  = pygame.image.load("mine.png")
ME_IMAGE = pygame.image.load("me.png")
SEA_IMAGE = pygame.image.load("sea.png")
FLAG_IMAGE = pygame.image.load("flag.png")


ENEMY_SIZE = 50
ME_SIZE = 50

ENEMY = pygame.transform.scale(ENEMY_IMAGE, (ENEMY_SIZE, ENEMY_SIZE))
ME = pygame.transform.scale(ME_IMAGE, (ME_SIZE, ME_SIZE))
SEA = pygame.transform.scale(SEA_IMAGE, (WIDTH, HEIGHT))
FLAG = pygame.transform.scale(FLAG_IMAGE, (ME_SIZE, ME_SIZE))






# ----------------------------------------------------------------------------
# třídy objektů 
# ----------------------------------------------------------------------------

# trida reprezentujici minu
class Mine:
    def __init__(self):

        # random x direction
        if random.random() > 0.5:
            self.dirx = 1
        else: 
            self.dirx = -1
            
        # random y direction    
        if random.random() > 0.5:
            self.diry = 1
        else: 
            self.diry = -1

        x = random.randint(200, WIDTH - ENEMY_SIZE) 
        y = random.randint(200, HEIGHT - ENEMY_SIZE) 
        self.rect = pygame.Rect(x, y, ENEMY_SIZE, ENEMY_SIZE)
        
        self.velocity = random.randint(1, MAX_MINE_VELOCITY)
        
  
    
  
# trida reprezentujici me, tedy meho agenta        
class Me:
    def __init__(self):
        self.rect = pygame.Rect(10, random.randint(1, 300), ME_SIZE, ME_SIZE)  
        self.alive = True
        self.won = False
        self.timealive = 0
        self.sequence = []
        self.fitness = 0
        self.dist = 0
        self.won_counter = 0
        self.timeWon = 0
    
    
# třída reprezentující cíl = praporek    
class Flag:
    def __init__(self):
        self.rect = pygame.Rect(WIDTH - ME_SIZE, HEIGHT - ME_SIZE - 10, ME_SIZE, ME_SIZE)
        

# třída reprezentující nejlepšího jedince - hall of fame   
class Hof:
    def __init__(self):
        self.sequence = []
        
    
    
# -----------------------------------------------------------------------------    
# nastavení herního plánu    
#-----------------------------------------------------------------------------
    

# rozestavi miny v danem poctu num
def set_mines(num):
    l = []
    for i in range(num):
        m = Mine()
        l.append(m)
        
    return l
    

# inicializuje me v poctu num na start 
def set_mes(num):
    l = []
    for i in range(num):
        m = Me()
        l.append(m)
        
    return l

# zresetuje vsechny mes zpatky na start
def reset_mes(mes, pop):
    for i in range(len(pop)):
        me = mes[i]
        me.rect.x = 10
        me.rect.y = 10
        me.alive = True
        me.dist = 0
        me.won = False
        me.timealive = 0
        me.sequence = pop[i]
        me.fitness = 0



    

# -----------------------------------------------------------------------------    
# senzorické funkce 
# -----------------------------------------------------------------------------    

# TODO

def Flag_senzor(me,flag):
    return abs(me.rect.x-flag.rect.x)+abs(me.rect.y-flag.rect.y)

def vzdalenostOdVlajky(me, flag):
    return abs(me.rect.x-flag.rect.x)+abs(me.rect.y-flag.rect.y)

def vzdalenostOdPraveHrany(me):
    return WIDTH - me.rect.x

def vzdalenostOdSpoda(me):
    return HEIGHT - me.rect.y
    
def Mine_senzor(me,mines):
    nejblizsimina=mines[0]    #vybere nejblizsi minu TODO
    return me.rect.x-nejblizsimina.rect.x, me.rect.y-nejblizsimina.rect.y

#def isDead(me):
 #   return me.timealive !=
    




# -----> ZDE je prostor pro vlastní senzorické funkce !!!!









# ---------------------------------------------------------------------------
# funkce řešící pohyb agentů
# ----------------------------------------------------------------------------


# konstoluje kolizi 1 agenta s minama, pokud je kolize vraci True
def me_collision(me, mines):    
    for mine in mines:
        if me.rect.colliderect(mine.rect):
            #pygame.event.post(pygame.event.Event(ME_HIT))
            return True
    return False
            
            
# kolidujici agenti jsou zabiti, a jiz se nebudou vykreslovat
def mes_collision(mes, mines):
    for me in mes: 
        if me.alive and not me.won:
            if me_collision(me, mines):
                me.alive = False
            
            
# vraci True, pokud jsou vsichni mrtvi Dave            
def all_dead(mes):    
    for me in mes: 
        if me.alive:
            return False
    
    return True


# vrací True, pokud již nikdo nehraje - mes jsou mrtví nebo v cíli
def nobodys_playing(mes):
    for me in mes: 
        if me.alive and not me.won:
            return False
    
    return True


# rika, zda agent dosel do cile
def me_won(me, flag):
    if me.rect.colliderect(flag.rect):
        return True
    
    return False


# vrací počet živých mes
def alive_mes_num(mes):
    c = 0
    for me in mes:
        if me.alive:
            c += 1
    return c



# vrací počet mes co vyhráli
def won_mes_num(mes):
    c = 0
    for me in mes: 
        if me.won:
            c += 1
    return c

         
    
# resi pohyb miny        
def handle_mine_movement(mine):
        
    if mine.dirx == -1 and mine.rect.x - mine.velocity < 0:
        mine.dirx = 1
       
    if mine.dirx == 1  and mine.rect.x + mine.rect.width + mine.velocity > WIDTH:
        mine.dirx = -1

    if mine.diry == -1 and mine.rect.y - mine.velocity < 0:
        mine.diry = 1
    
    if mine.diry == 1  and mine.rect.y + mine.rect.height + mine.velocity > HEIGHT:
        mine.diry = -1
         
    mine.rect.x += mine.dirx * mine.velocity
    mine.rect.y += mine.diry * mine.velocity


# resi pohyb min
def handle_mines_movement(mines):
    for mine in mines:
        handle_mine_movement(mine)


#----------------------------------------------------------------------------
# vykreslovací funkce 
#----------------------------------------------------------------------------


# vykresleni okna
def draw_window(mes, mines, flag, level, generation, timer):
    WIN.blit(SEA, (0, 0))   
    
    t = LEVEL_FONT.render("level: " + str(level), 1, WHITE)   
    WIN.blit(t, (10  , HEIGHT - 30))
    
    t = LEVEL_FONT.render("generation: " + str(generation), 1, WHITE)   
    WIN.blit(t, (150  , HEIGHT - 30))
    
    t = LEVEL_FONT.render("alive: " + str(alive_mes_num(mes)), 1, WHITE)   
    WIN.blit(t, (350  , HEIGHT - 30))
    
    t = LEVEL_FONT.render("won: " + str(won_mes_num(mes)), 1, WHITE)   
    WIN.blit(t, (500  , HEIGHT - 30))
    
    t = LEVEL_FONT.render("timer: " + str(timer), 1, WHITE)   
    WIN.blit(t, (650  , HEIGHT - 30))
    
    
    
    
    

    WIN.blit(FLAG, (flag.rect.x, flag.rect.y))    
         
    # vykresleni min
    for mine in mines:
        WIN.blit(ENEMY, (mine.rect.x, mine.rect.y))
        
    # vykresleni me
    for me in mes: 
        if me.alive:
            WIN.blit(ME, (me.rect.x, me.rect.y))
        
    pygame.display.update()



def draw_text(text):
    t = BOOM_FONT.render(text, 1, WHITE)   
    WIN.blit(t, (WIDTH // 2  , HEIGHT // 2))     
    
    pygame.display.update()
    pygame.time.delay(1000)







#-----------------------------------------------------------------------------
# funkce reprezentující neuronovou síť, pro inp vstup a zadané váhy wei, vydá
# čtveřici výstupů pro nahoru, dolu, doleva, doprava    
#----------------------------------------------------------------------------


# <----- ZDE je místo vlastní funkci !!!!

I = 7 # počet vstupních neuronů
N = 50 # počet neuronů ve skryte vrstve
O = 4 # počet vystupnich neuronu
# funkce reprezentující výpočet neuronové funkce
# funkce dostane na vstupu vstupy neuronové sítě inp, a váhy hran wei
# vrátí seznam hodnot výstupních neuronů

#import numpy as np

#def neural_network(inp, wei):
    # Výpočet aktivací vstupních neuronů
    #a1 = inp
    
    # Výpočet aktivací neuronů ve skryté vrstvě
    #z2 = np.dot(a1, wei[0]) # váhový součet
    #a2 = 1 / (1 + np.exp(-z2)) # sigmoidální aktivační funkce

    # Výpočet aktivací výstupních neuronů
    #z3 = np.dot(a2, wei[1]) # váhový součet
    #a3 = 1 / (1 + np.exp(-z3)) # sigmoidální aktivační funkce
    
    # Výstupní hodnoty neuronové sítě
    #return a3.tolist()
    
    
SKRYTYCH_NEURONU=20
VYSTUPU=4
def nn_function(inp, wei):
    mezi=[]
    vaha=0

    for neuroni in range(0,SKRYTYCH_NEURONU):#pro kazdy neuron
        sumavah=0
        for vstupi in range(0,len(inp)):#secte vstupy vynasobene vahami
            sumavah+=inp[vstupi]*wei[vaha]
            vaha+=1
        if(sumavah>wei[len(wei)-SKRYTYCH_NEURONU+neuroni]):
            mezi.append(sumavah)
        else:
            mezi.append(0)

    out=[]
    for vystupi in range(0,VYSTUPU):#pro kazdy vystupni neuron
        suma=0
        for i in range(0,SKRYTYCH_NEURONU):#projde pro kazdy vystupneuronu
            suma+=wei[vaha]*mezi[i]
            vaha+=1
        out.append(suma)
        
    #print(vystup)
    return out


def nn_function2(inp, wei):
    n = []
    #print(inp)
    for i in range(N):
        
        # Definování počáteční pozice podpole a jeho délky
        start_index = I * i
        end_index = start_index + I

        # Výběr podpole z pole 'wei' pomocí definovaných indexů
        selected_subarray = wei[start_index:end_index]

        # Přiřazení výsledného podpole do proměnné 'lwei'
        lwei = selected_subarray
        
        l = [ lwei[r] * inp[r]  for r in range(I)]
        n.append(sum(l))
        
    # Definování počáteční pozice podpole a jeho délky
    start_index = N * I
    end_index = start_index + N

    # Výběr podpole z pole 'wei' pomocí definovaných indexů
    selected_subarray = wei[start_index:end_index]

    # Přiřazení výsledného podpole do proměnné 'thresholds'
    thresholds = selected_subarray
    for i in range(N):
        if n[i] >= thresholds[i]:
            n[i] = 1.0
        else:
            n[i] = 0.0
    
    rwei = wei[(I+1)*N:]
    out = []
    
    for i in range(O):
        # Definování počáteční pozice podpole a jeho délky
        start_index = i * N
        end_index = start_index + N

        # Výběr podpole z pole 'rwei' pomocí definovaných indexů
        selected_subarray = rwei[start_index:end_index]

        # Přiřazení výsledného podpole do proměnné 'lwei'
        lwei = selected_subarray
        l = [lwei[r] * n[r] for r in range(N)]
        out.append(sum(l))
  
    
    return out


# naviguje jedince pomocí neuronové sítě a jeho vlastní sekvence v něm schované
def nn_navigate_me(me, inp):

    # TODO  <------ ZDE vlastní kód vyhodnocení výstupů z neuronové sítě !!!!!!
    out = np.array(nn_function2(inp, me.sequence))
    max = out[0]
    index = 0
    for i in range(1,len(out)):
        if out[i] > max:
            max = out[i]
            index = i
    ind = index
    
    #print(out, ind)
    #print(vzdalenostOdVlajky(me))
    

    
    # nahoru, pokud není zeď
    if ind == 0 and me.rect.y - ME_VELOCITY > 0:
        me.rect.y -= ME_VELOCITY
        
        me.dist += ME_VELOCITY
    
    # dolu, pokud není zeď
    if ind == 1 and me.rect.y + me.rect.height + ME_VELOCITY < HEIGHT:
        me.rect.y += ME_VELOCITY  
        me.dist += ME_VELOCITY
    
    # doleva, pokud není zeď
    if ind == 2 and me.rect.x - ME_VELOCITY > 0:
        me.rect.x -= ME_VELOCITY
        me.dist += ME_VELOCITY
        
    # doprava, pokud není zeď    
    if ind == 3 and me.rect.x + me.rect.width + ME_VELOCITY < WIDTH:
        me.rect.x += ME_VELOCITY
        me.dist += ME_VELOCITY
    
    
        

# updatuje, zda me vyhrali 
def check_mes_won(mes, flag):
    for me in mes: 
        if me.alive and not me.won:
            if me_won(me, flag):
                me.won = True
                
    
def number_of_won(mes, flag):
    for me in mes: 
        if me.alive:
            if me_won(me,flag):
                me.won_counter =+ 1
                
            
# resi pohyb mes
def handle_mes_movement(mes, mines, flag):
    
    for me in mes:

        if me.alive and not me.won:
            
            # <----- ZDE  sbírání vstupů ze senzorů !!!
            # naplnit vstup in vstupy ze senzorů
            inp = []
            
            inp.append(me.rect.x)
            inp.append(me.rect.y)
            
            inp.append(Flag_senzor(me, flag))
            
            x, y = Mine_senzor(me, mines)
            inp.append(x)
            inp.append(y)
            
            inp.append(vzdalenostOdPraveHrany(me))
            inp.append(vzdalenostOdSpoda(me))
            
            
            nn_navigate_me(me, inp)
            
            #print(Flag_senzor(me, flag), me.rect.x, me.rect.y)



# updatuje timery jedinců
def update_mes_timers(mes, timer):
    for me in mes:
        if me.alive and not me.won:
            me.timealive = timer
            
def update_mes_won_timers(mes, timer):
    for me in mes:
        if me.alive and me.won:
            me.timeWon = timer



# ---------------------------------------------------------------------------
# fitness funkce výpočty jednotlivců
#----------------------------------------------------------------------------


# funkce pro výpočet fitness všech jedinců
def handle_mes_fitnesses(mes, flag, mines):    
    
    #vzdalenostOdVlajky = 0.3
    #vyhralVaha = 0.3
    #vahaCasVyhry = 0.3 
    #timeAliveVaha = 0.2
    #nejblizsimina=mines[0]    #vybere nejblizsi minu TODO
    # <--------- TODO  ZDE se počítá fitness jedinců !!!!
    # na základě informací v nich uložených, či jiných vstupů
    
    for me in mes: 
        me.fitness=0
        #me.fitness += me.timealive * 0.1 #0-1000
        #me.fitness += me.dist * -0.1#0-1400
        #me.fitness -= me.rect.x  * 0.1
        #me.fitness += me.rect.y 
        #me.fitness += (abs(me.rect.x-flag.rect.x)+abs(me.rect.y-flag.rect.y)) * vzdalenostOdVlajky
        #me.fitness += me.rect.x-nejblizsimina.rect.x *0.1
        #me.fitness += me.rect.x-nejblizsimina.rect.y *0.1
        
        #kdyz trefi minu fitness -
        #print(vzdalenostOdVlajky(me, flag))
        me.fitness -= (abs(me.rect.x-flag.rect.x)+abs(me.rect.y-flag.rect.y)) * 0.1
        #me.fitness -= vzdalenostOdVlajky(me, flag) * 0.1
        if me.alive:
            me.fitness += 1000 * 0.5
        else:
            me.fitness -= 1000 * 0.05
        
        if me.won:
            me.fitness += 1000 * 5
        else:
            me.fitness -= 1000 * 0.5
            
        #print(me.fitness)
        #me.fitness += me.won_counter * 0.3
        #me.fitness = vzdalenostOdPraveHrany(me) * -0.3
        #me.fitness += vzdalenostOdSpoda(me) * -0.3
        #me.fitness += me.timeWon * 0.4
        
        
        
        
        
        

    
    

# uloží do hof jedince s nejlepší fitness
def update_hof(hof, mes):
    l = [me.fitness for me in mes]
    ind = np.argmax(l)
    hof.sequence = mes[ind].sequence.copy()
    

# ----------------------------------------------------------------------------
# main loop 
# ----------------------------------------------------------------------------

def main():
    
    
    # =====================================================================
    # <----- ZDE Parametry nastavení evoluce !!!!!
    
    VELIKOST_POPULACE = 50
    EVO_STEPS = 5  # pocet kroku evoluce
    DELKA_JEDINCE = I * N + N * O + N  # <--------- záleží na počtu vah a prahů u neuronů !!!!!
    NGEN = 30        # počet generací
    CXPB = 0.6          # pravděpodobnost crossoveru na páru
    MUTPB = 0.2        # pravděpodobnost mutace
    
    SIMSTEPS = 500
    
    creator.create("FitnessMax", base.Fitness, weights=(-1.0,))
    
    creator.create("Individual", list, fitness=creator.FitnessMax)
    
    toolbox = base.Toolbox()

    toolbox.register("attr_rand", random.random)
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_rand, DELKA_JEDINCE)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    # vlastni random mutace
    # <----- ZDE TODO vlastní mutace
    def mutRandom(individual, indpb):
        for i in range(len(individual)):
            if random.random() < indpb:
                individual[i] = random.random()
        return individual,

    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", mutRandom, indpb=0.05)
    toolbox.register("select", tools.selRoulette)
    toolbox.register("selectbest", tools.selBest)
    
    pop = toolbox.population(n=VELIKOST_POPULACE)
        
    # =====================================================================
    
    clock = pygame.time.Clock()

    
    
    # =====================================================================
    # testování hraním a z toho odvození fitness 
   
    
    mines = []
    mes = set_mes(VELIKOST_POPULACE)    
    flag = Flag()
    
    hof = Hof()
    
    
    run = True

    level = 1   # <--- ZDE nastavení obtížnosti počtu min !!!!!
    generation = 0
    
    evolving = True
    evolving2 = False
    timer = 0
    
    while run:  
        
        clock.tick(FPS)
        
               
        # pokud evolvujeme pripravime na dalsi sadu testovani - zrestartujeme scenu
        if evolving:           
            timer = 0
            generation += 1
            reset_mes(mes, pop) # přiřadí sekvence z populace jedincům a dá je na start !!!!
            mines = set_mines(level) 
            evolving = False
            
        timer += 1    
            
        check_mes_won(mes, flag)
        handle_mes_movement(mes, mines, flag)
        
        
        handle_mines_movement(mines)
        
        mes_collision(mes, mines)
        
        if all_dead(mes):
            evolving = True
            #draw_text("Boom !!!")"""

            
        update_mes_timers(mes, timer)        
        draw_window(mes, mines, flag, level, generation, timer)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
    

        
        
        
        
        # ---------------------------------------------------------------------
        # <---- ZDE druhá část evoluce po simulaci  !!!!!
        
        # druhá část evoluce po simulaci, když všichni dohrají, simulace končí 1000 krocích

        if timer >= SIMSTEPS or nobodys_playing(mes): 
            
            # přepočítání fitness funkcí, dle dat uložených v jedinci
            handle_mes_fitnesses(mes,flag, mines)   # <--------- ZDE funkce výpočtu fitness !!!!
            
            update_hof(hof, mes)
            
            
            #plot fitnes funkcí
            #ff = [me.fitness for me in mes]
            
            #print(ff)
            
            # přiřazení fitnessů z jedinců do populace
            # každý me si drží svou fitness, a každý me odpovídá jednomu jedinci v populaci
            for i in range(len(pop)):
                ind = pop[i]
                me = mes[i]
                ind.fitness.values = (me.fitness, )
            
            
            # selekce a genetické operace
            offspring = toolbox.select(pop, len(pop))
            offspring = list(map(toolbox.clone, offspring))
            

            
            
            

            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() < CXPB:
                    toolbox.mate(child1, child2)

            for mutant in offspring:
                if random.random() < MUTPB:
                    toolbox.mutate(mutant)  
            
            pop[:] = offspring
            
            
            evolving = True
                   
        
    # po vyskočení z cyklu aplikace vytiskne DNA sekvecni jedince s nejlepší fitness
    # a ukončí se
    
    pygame.quit()    


if __name__ == "__main__":
    main()