# -*- coding: utf-8 -*-
import pygame
import random
import numpy as np
from collections import deque #umi se chovat jako priotini fronta - first in, first out

BLOCKTYPES = 5

# třída reprezentující prostředí
class Env:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.arr = np.zeros((height, width), dtype=int)
        self.startx = 0
        self.starty = 0
        self.goalx = width-1
        self.goaly = height-1
        
    def is_valid_xy(self, x, y):      
        if x >= 0 and x < self.width and y >= 0 and y < self.height and self.arr[y, x] == 0:
            return True
        return False 
        
    def set_start(self, x, y):
        if self.is_valid_xy(x, y):
            self.startx = x
            self.starty = y
            
    def set_goal(self, x, y):
        if self.is_valid_xy(x, y):
            self.goalx = x
            self.goaly = y
               
        
    def is_empty(self, x, y):
        if self.arr[y, x] == 0:
            return True
        return False
    
        
    def add_block(self, x, y):
        if self.arr[y, x] == 0:
            r = random.randint(1, BLOCKTYPES)
            self.arr[y, x] = r
                
    def get_neighbors(self, x, y):
        l = []
        if x-1 >= 0 and self.arr[y, x-1] == 0:
            l.append((x-1, y))
        
        if x+1 < self.width and self.arr[y, x+1] == 0:
            l.append((x+1, y))
            
        if y-1 >= 0 and self.arr[y-1, x] == 0:
            l.append((x, y-1))
        
        if y+1 < self.height and self.arr[y+1, x] == 0:
            l.append((x, y+1))
        
        return l
        
     
    def get_tile_type(self, x, y):
        return self.arr[y, x]
    
    
    # vrací dvojici 1. frontu dvojic ze startu do cíle, 2. seznam dlaždic
    # k zobrazení - hodí se např. pro zvýraznění cesty, nebo expandovaných uzlů
    # start a cíl se nastaví pomocí set_start a set_goal
    # <------    ZDE vlastní metoda
    def path_planner(self, start, goal):        
        currentNode = start
        expanded = [] #expandovane uzly
        ofina = [] #aktualni uzly
        ofina.append((start[0], start[1], 0)) #start x , y
        print(start, ofina)
        
        for i in range(0, 1500):#range?
            expanded.append(currentNode) #ulozeny zaznam
            if(currentNode[0] == goal[0] and currentNode[1] == goal[1]): 
                print("Cil nalezen")
                break
            if(len(ofina) == 0):
                break
            currentNode = ofina[0]#pouze, kdyz je ofina spravne serazena
            ofina.remove(currentNode)
            
            for i in self.get_neighbors(currentNode[0], currentNode[1]):
               #podminky prioritni fronty - vrazeni hodnot
               #vzdalenost od startu
               
               #neexpandovat uzly, ktere uz jsem expandoval v minulosti
               isAlreadyExpanded = False
               for k in expanded:
                   if(k[0] == i[0] and k[1] == i[1]):
                       isAlreadyExpanded = True
                       break
                      
               if (not isAlreadyExpanded):   
                   if(False):#Dijsktruv algoritmus
                       wasAdded = False
                       for j in range(0,len(ofina)):
                           if(ofina[j][2] < currentNode[2] + 1):#Vkládám před
                               ofina.insert(j, (i[0], i[1], currentNode[2] + 1, currentNode[0], currentNode[1]))
                               wasAdded = True
                               break
                       if(not wasAdded):
                           ofina.append((i[0], i[1], currentNode[2] + 1, currentNode[0], currentNode[1]))
                   elif(False):#Hladový algoritmus
                       wasAdded = False
                       for j in range(0,len(ofina)):
                           #body řazeny dle vzdálenosti do cíle od nekratší
                           if(abs(ofina[j][0] - goal[0]) + abs(ofina[j][1] - goal[1]) > abs(i[0] - goal[0]) + abs(i[1] - goal[1])):
                               ofina.insert(j, (i[0], i[1], currentNode[2] + 1, currentNode[0], currentNode[1]))
                               wasAdded = True
                               break
                       if(not wasAdded):
                           ofina.append((i[0], i[1], currentNode[2] + 1, currentNode[0], currentNode[1]))
                   else:#A*
                       wasAdded = False
                       for j in range(0,len(ofina)):
                           
                           if(ofina[j][2] + (abs(ofina[j][0] - goal[0]) + abs(ofina[j][1] - goal[1])) > (currentNode[2] + 1) + (abs(i[0] - goal[0]) + abs(i[1] - goal[1]))):
                           #if(ofina[j][2] + (abs(ofina[j][0] - goal[0])) > (currentNode[2] + 1) + (abs(i[0] - goal[0]))):
                               ofina.insert(j, (i[0], i[1], currentNode[2] + 1, currentNode[0], currentNode[1]))
                               wasAdded = True
                               break
                       if(not wasAdded):
                           ofina.append((i[0], i[1], currentNode[2] + 1, currentNode[0], currentNode[1]))
            
            
            
            #pridat do ofiny platne sousedy + vradit je tam na zaklade zvoleho algoritmu
            #nevracet se odkud jsem prisel
            #zaznamenavat cestu
            
            
            
            
        # přímo zadrátovaná cesta z bodu (1, 0) do (9, 7)
        d = deque()
        
        #if(not expanded[0]):
        #d.appendleft((expanded[len(expanded) - 1][0], expanded[len(expanded) - 1][1]))
        d.appendleft(goal)
        #while(d[0][0] != start[0] and d[0][1] != start[1]):
        for j in range(0,1000):
            for i in expanded:
                if(d[0][0] == i[0] and d[0][1] == i[1]):
                    d.appendleft((i[3], i[4]))
                    break#break, prvek byl nalezen, netřeba hledat dál
            if(d[0][0] == start[0] and d[0][1] == start[1]):#místo != má být ==
                break
        testArray = deque()
        
        for i in expanded:
             testArray.append((i[0],i[1]))
        
        
        # path=deque()
        # if(True):
        #     i=len(expanded)-1
        #     for iii in range(0,len(expanded)):
        #         if(expanded[i][0]==start[0] and expanded[i][1]==start[1]):
        #             path.appendleft((expanded[i][0],expanded[i][1]))
        #         if(len(expanded[i])<6):
        #             break
        #         ascendant=(expanded[i][4],expanded[i][5])
        #         path.appendleft((expanded[i][0],expanded[i][1]))
        #         if(expanded[i][0]==start[0] and expanded[i][1]==start[1]):
        #             break
        #         for q in range(0,len(expanded)):
        #             if(expanded[q][0]==ascendant[0] and expanded[q][1]==ascendant[1]):
        #                 i=q
        #                 break
        # d.appendleft((9, 7))
        # d.appendleft((9, 6))
        # d.appendleft((9, 5))
        # d.appendleft((9, 4))
        # d.appendleft((9, 3))
        # d.appendleft((9, 2))
        # d.appendleft((9, 1))
        # d.appendleft((9, 0))
        # d.appendleft((8, 0))
        # d.appendleft((7, 0))
        # d.appendleft((6, 0))
        # d.appendleft((5, 0))
        # d.appendleft((4, 0))
        # d.appendleft((3, 0))
        # d.appendleft((2, 0))
        # d.appendleft((1, 0))
                
        return d, list(testArray)
    
       
        
# třída reprezentující ufo        
class Ufo:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.path = deque()
        self.tiles = []
    
   
    # přemístí ufo na danou pozici - nejprve je dobré zkontrolovat u prostředí, 
    # zda je pozice validní
    def move(self, x, y):
        self.x = x
        self.y = y

   
    
    # reaktivní navigace <------------------------ !!!!!!!!!!!! ZDE DOPLNIT
    def reactive_go(self, env):
        r = random.random()
        
        dx = 0
        dy = 0
        
        if r > 0.5: 
            r = random.random()
            if r < 0.5:
                dx = -1
            else:
                dx = 1
            
        else:
            r = random.random()
            if r < 0.5:
                dy = -1
            else:
                dy = 1
        
        return (self.x + dx, self.y + dy)
        
    
    # nastaví cestu k vykonání 
    def set_path(self, p, t=[]):
        self.path = p
        self.tiles = t
   
    
    # vykoná naplánovanou cestu, v každém okamžiku na vyzvání vydá další
    # way point 
    def execute_path(self):
        if self.path:
            return self.path.popleft()
        return (-1, -1)
       




# definice prostředí -----------------------------------

TILESIZE = 50



#<------    definice prostředí a překážek !!!!!!

WIDTH = 12
HEIGHT = 9

env = Env(WIDTH, HEIGHT)

env.add_block(1, 1)
env.add_block(2, 2)
env.add_block(3, 3)
env.add_block(4, 4)
env.add_block(5, 5)
env.add_block(6, 6)
env.add_block(7, 7)
env.add_block(8, 8)
env.add_block(0, 8)

env.add_block(11, 1)
env.add_block(11, 6)
env.add_block(1, 3)
env.add_block(2, 4)
env.add_block(4, 5)
env.add_block(2, 6)
env.add_block(3, 7)
env.add_block(4, 8)
env.add_block(0, 8)


env.add_block(1, 8)
env.add_block(2, 8)
env.add_block(3, 5)
env.add_block(4, 8)
env.add_block(5, 6)
env.add_block(6, 4)
env.add_block(7, 2)
env.add_block(8, 1)


# pozice ufo <--------------------------
ufo = Ufo(env.startx, env.starty)

WIN = pygame.display.set_mode((env.width * TILESIZE, env.height * TILESIZE))

pygame.display.set_caption("Block world")

pygame.font.init()

WHITE = (255, 255, 255)



FPS = 2



# pond, tree, house, car

BOOM_FONT = pygame.font.SysFont("comicsans", 100)   
LEVEL_FONT = pygame.font.SysFont("comicsans", 20)   


TILE_IMAGE = pygame.image.load("tile.jpg")
MTILE_IMAGE = pygame.image.load("markedtile.jpg")
HOUSE1_IMAGE = pygame.image.load("house1.jpg")
HOUSE2_IMAGE = pygame.image.load("house2.jpg")
HOUSE3_IMAGE = pygame.image.load("house3.jpg")
TREE1_IMAGE  = pygame.image.load("tree1.jpg")
TREE2_IMAGE  = pygame.image.load("tree2.jpg")
UFO_IMAGE = pygame.image.load("ufo.jpg")
FLAG_IMAGE = pygame.image.load("flag.jpg")


TILE = pygame.transform.scale(TILE_IMAGE, (TILESIZE, TILESIZE))
MTILE = pygame.transform.scale(MTILE_IMAGE, (TILESIZE, TILESIZE))
HOUSE1 = pygame.transform.scale(HOUSE1_IMAGE, (TILESIZE, TILESIZE))
HOUSE2 = pygame.transform.scale(HOUSE2_IMAGE, (TILESIZE, TILESIZE))
HOUSE3 = pygame.transform.scale(HOUSE3_IMAGE, (TILESIZE, TILESIZE))
TREE1 = pygame.transform.scale(TREE1_IMAGE, (TILESIZE, TILESIZE))
TREE2 = pygame.transform.scale(TREE2_IMAGE, (TILESIZE, TILESIZE))
UFO = pygame.transform.scale(UFO_IMAGE, (TILESIZE, TILESIZE))
FLAG = pygame.transform.scale(FLAG_IMAGE, (TILESIZE, TILESIZE))




        
        
        

def draw_window(ufo, env):

    for i in range(env.width):
        for j in range(env.height):
            t = env.get_tile_type(i, j)
            if t == 1:
                WIN.blit(TREE1, (i*TILESIZE, j*TILESIZE))
            elif t == 2:
                WIN.blit(HOUSE1, (i*TILESIZE, j*TILESIZE))
            elif t == 3:
                WIN.blit(HOUSE2, (i*TILESIZE, j*TILESIZE))
            elif t == 4:
                WIN.blit(HOUSE3, (i*TILESIZE, j*TILESIZE))  
            elif t == 5:
                WIN.blit(TREE2, (i*TILESIZE, j*TILESIZE))     
            else:
                WIN.blit(TILE, (i*TILESIZE, j*TILESIZE))
    
        
    for (x, y) in ufo.tiles:
        WIN.blit(MTILE, (x*TILESIZE, y*TILESIZE))
        
    
    WIN.blit(FLAG, (env.goalx * TILESIZE, env.goaly * TILESIZE))        
    WIN.blit(UFO, (ufo.x * TILESIZE, ufo.y * TILESIZE))
        
    pygame.display.update()
    
    
    

def main():
    
    
    #  <------------   nastavení startu a cíle prohledávání !!!!!!!!!!
    env.set_start(0, 0)
    env.set_goal(9, 7)
    
    
    p, t = env.path_planner((env.startx, env.starty), (env.goalx, env.goaly))   # cesta pomocí path_planneru prostředí
    ufo.set_path(p, t)
    # ---------------------------------------------------
    
    
    clock = pygame.time.Clock()
    
    run = True
    go = False    
    
    while run:  
        
        clock.tick(FPS)
        

        # <---- reaktivní pohyb dokud nedojde do cíle 
        if (ufo.x != env.goalx) or (ufo.y != env.goaly):        
            #x, y = ufo.reactive_go(env)
            
            x, y = ufo.execute_path()
            
            if env.is_valid_xy(x, y):
                ufo.move(x, y)
            else:
                print('[', x, ',', y, ']', "wrong coordinate !")
        

                        
        draw_window(ufo, env)
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
    
    pygame.quit()    


if __name__ == "__main__":
    main()